# ⟂-Loss

This repository provides code for the publication "⊥-loss: a symmetric loss function for magnetic resonance imaging reconstruction and image registration with deep learning" (https://doi.org/10.1016/j.media.2022.102509)

## Structure
This project provides three files: 
- loss_surface simulation.jl
- PerpLoss - Image reconstruction.ipynb
- PerpLoss - Image registration.ipynb

The first file numerically computes the loss surface of the L1, L2, and ⊥+L2-loss, as shown in Figure 3 in the manuscript.

The second file shows an example of how ⊥+L2-loss or ⊥+SSIM-loss can be used for image reconstruction. Specifically, the model trains a E2E-VarNet (Sriram et al., 2020) to reconstruct data of the Multi-channel MR Reconstruction (MC-MRRec) Challenge (Beauferris et al., 2020). 

Finally, the third file shows an example of how ⊥+L2-loss can be used to estimate deformation vector fields (DVFs), which are useful for image registration. This file approximately implements the image registration experiment in the manuscript.  


## Reproducibility
This code only illustrates the properties of ⊥-loss and how they could be used for regression. This code does not exactly reproduce the experiments in the manuscript. 

## License 
MIT License

Copyright (c) 2022 Maarten Terpstra

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
