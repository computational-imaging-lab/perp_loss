using PyPlot
using StaticArrays
using LinearAlgebra
import Base.abs, Base.<
using StructArrays
using ThreadsX
using Seaborn
using PyCall
axes_grid1 = pyimport("mpl_toolkits.axes_grid1")

# Define a vector in 2D
struct Vector2D{T} <: FieldVector{2, T}
    x::T
    y::T
end

abs(r::Vector2D) = Vector2D(abs(r.x), abs(r.y))

# Vector rotation by some angle in radians
function rotate_vector(v::Vector2D, θ)::Vector2D
    cs = cos(θ);
    sn = sin(θ);

    xr = v.x * cs - v.y * sn; 
    yr = v.x * sn + v.y * cs;
    return Vector2D(xr, yr)
end

# Find the angle between two vectors
function angle(v1::Vector2D, v2::Vector2D)
    p = norm(v1) * norm(v2)
    if p < 1e-4
        p = 1e-4
    end
    θ = dot(v1, v2) / p
    return acos(θ)
end

# define loss functions
epe(v1, v2) = norm(v1-v2)
l1(v1, v2) = norm(v1-v2,1)
magnitude_error(v1, v2) = abs(norm(v1) - norm(v2))
mag_error_l2(v1, v2) = (norm(v1) - norm(v2)) ^ 2

# The proposed ⟂-Loss 
function pdist(target, est, θ)
    # Find magnitudes
    l_est = norm(est)
    l_tar = norm(target)
    
    # Disregard angle for small estimated vectors
    if l_est < 1e-4
        return mag_error_l2(est, target)
    end

    # ⟂ part
    cross = target[1] * est[2] - est[1] * target[2]
    ploss = abs(cross) / l_est

    # Handle angles > 90 degrees. Overstreched reduces the ⟂ part,
    # so we compensate as described in the paper
    if abs(θ) > π/2.0
        ploss = l_tar + ((l_tar - ploss))
    end
    
    return (ploss + mag_error_l2(est, target))
end

# Collection of losses for one sampled vector pair
struct SampleResult{T} <: FieldVector{3,T}
    epe::T
    l1::T
    pdist::T
end

abs(r::SampleResult) = SampleResult(abs(r.epe), abs(r.l1), abs(r.pdist))

(<)(a::SampleResult, b::SampleResult)::SampleResult{Bool} = SampleResult(a.epe < b.epe, a.l1 < b.l1, a.pdist < b.pdist)
(<)(a::SampleResult, b::Float64)::SampleResult{Bool} = SampleResult(a.epe < b, a.l1 < b, a.pdist < b)

# Run the simulation: Generate random vector within the pts domain
# with a random magnitude and orientation. The paired vector is generated
# to have a known magnitude scale error and angle error. Then we compute the normalized losses
function vector_sample(θ, scale, pts_range)
    mag = 0.0
    
    v = Vector2D(rand(pts_range), rand(pts_range))

    # Target vector should be long enough so try again
    while mag < 1e-3
        v = Vector2D(rand(pts_range), rand(pts_range))
        mag = norm(v)
    end
    v_p = rotate_vector(v, θ) * scale

    epe_loss = epe(v, v_p) / mag
    l1_loss = l1(v, v_p) / mag
    pdist_loss = pdist(v, v_p, θ) / mag

    return SampleResult(epe_loss, l1_loss, pdist_loss)
end

# Generate iter vector per (θ, λ) pair and compute mean loss
function run_simulation(sz, iters, min_scale, max_scale, min_angle, max_angle)
    scales = range(min_scale, stop=max_scale, length=sz)
    angles = range(min_angle, stop=max_angle, length=sz)

    pts_range = range(-5, 5, length=1001)
    mean_loss = (θ, λ) -> sum(vector_sample(θ, λ, pts_range) for _ in 1:iters) / iters
    output = ThreadsX.collect(mean_loss(θ, λ) for θ ∈ angles, λ ∈ scales)
    return scales, angles, output
end

# plot the results
function show_heatmap(f, ax, x, y, z, title, cmap="magma",vmin=0.0, vmax=5.0, xlabel=nothing, show_contours=true, show_ylabel=true, show_colorbar=true)
    q = ax.imshow(z, cmap=cmap, interpolation="none", extent=[x[1], x[end], y[end], y[1]], vmin=vmin, vmax=vmax, aspect="auto")
    ax.invert_yaxis()
    if xlabel == nothing
        xlabel = "\$\\lambda\$"
    end
    ax.set_xlabel(xlabel)
    if show_ylabel
        ax.set_ylabel("\$\\varphi\$ (rad)")
    end
    ax.set_title(title, pad=10)
    if show_contours==true
        ax.contour(x, y, z)
    end
    if show_colorbar
        plt.subplots_adjust(bottom=0.15)

        cax = f.add_axes([ax.get_position().x1+0.01,ax.get_position().y0,0.02,ax.get_position().height])
        plt.colorbar(q, cax=cax)
    end
end

function main(num_thetas_lambdas, n_samples)
    scales, angles, output = run_simulation(num_thetas_lambdas, n_samples, 0.05, 1.95, 0, π)

    set_context("poster")
    f, ax = plt.subplots(1, 3, figsize=(20, 6), squeeze=false)
    angle_ticks = [0, π/4, π/2, 3π/4, π]
    angle_ticklabels = ["0", "\$\\frac{π}{4}\$", "\$\\frac{π}{2}\$", "\$\\frac{3π}{4}\$", "\$π\$"]

    vmax = 4.0

    show_heatmap(f, ax[1, 1], scales, angles, map((x) -> x.l1, output), "\$\\ell^1_\\mathbb{C}\$","magma", 0.0, vmax, nothing, true, true, false)
    show_heatmap(f, ax[1, 2], scales, angles, map((x) -> x.epe, output), "\$\\ell^2_\\mathbb{C}\$", "magma", 0.0, vmax, nothing, true, false, false)
    show_heatmap(f, ax[1, 3], scales, angles, map((x) -> x.pdist, output), "⊥+\$\\ell^2\$-Loss", "magma", 0.0, vmax, nothing, true, false, true)
    [a.set_yticks(angle_ticks) for a in ax];
    [a.set_yticklabels(angle_ticklabels) for a in ax];
    f.savefig("figures/loss_surface_with_l1.png", format="png", dpi=300, pad_inches=0, bbox_inches="tight")
    f.savefig("figures/loss_surface_with_l1.eps", format="eps", dpi=300, pad_inches=0, bbox_inches="tight")

    plt.show()
end

main(101, 50001)
